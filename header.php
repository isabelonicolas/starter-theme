<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo( 'charset' ) ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head() ?>
	</head>
	<body <?php body_class() ?>>
		<?php $theme_settings = get_theme_settings() ?>

		<div id="app" class="app">
			<header class="app-header">
				<nav class="navigation-bar">
					<div class="navigation-bar__container">
						<?php if ( has_nav_menu( 'main-navigation' ) ) : ?>
							<a href="<?php echo site_url() ?>" class="navigation-bar__brand">
								<img src="<?php echo $theme_settings['header_logo']['url'] ?>" alt="<?php echo $theme_settings['header_logo']['alt'] ?>" />
							</a>
						<?php endif ?>
						<?php
							if ( has_nav_menu( 'main-navigation' ) ) {
								wp_nav_menu([
									'container_class' => 'navigation-bar__menu',
									'theme_location' => 'main-navigation'
								]);
							}
						?>
						<button class="navigation-bar__menu-toggle">
							<div>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</button>
					</div>
				</nav>
			</header>
			<div class="after-app-header">
				<?php if ( !is_search() && !is_single() && !is_404() ) : ?>
					<?php get_component('banner') ?>
				<?php endif ?>
			</div>
			<main class="app-main">