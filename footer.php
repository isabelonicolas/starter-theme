			</main>
			<footer class="app-footer">
				<div class="container">
					<div class="g-text-center">
						<span><?php echo 'Copyright &copy; ' . date('Y') . ' ' . get_bloginfo() . ' All rights reserved.' ?></span>
						<span><a href="<?php the_permalink(get_option( 'wp_page_for_privacy_policy' )) ?>">Privacy Policy</a></span>
					</div>
				</div>
			</footer>
		</div>
		<?php wp_footer() ?>
	</body>
</html>