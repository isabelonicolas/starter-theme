export default class NavigationBar {
  constructor(args) {
    this.init(args)
  }

  init = (args) => {
    this.windowEvents()
    // console.log(args)
  }

  windowEvents = () => {
    window.addEventListener("load", this.handleAppHeaderType)
    window.addEventListener("scroll", this.handleAppHeaderType)

    window.addEventListener("load", this.handleNavigationBarType)
    window.addEventListener("resize", this.handleNavigationBarType)

    window.addEventListener("load", this.handleNavigationBarDesign)
    window.addEventListener("scroll", this.handleNavigationBarDesign)

    window.addEventListener('load', this.handleNavigationBarMenuToggle)

    window.addEventListener('load', this.handleMobileLayout)
    window.addEventListener('resize', this.handleMobileLayout)
  }

  handleAppHeaderType = () => {
    
  }

  handleNavigationBarDesign = () => {
    const appHeader = document.querySelector(".app-header"),
          navigationBar = document.querySelector(".navigation-bar")
    
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
        height = appHeader.offsetHeight

    if ( scrollTop > height  ) {
      navigationBar.classList.add('navigation-bar--design2')
    } else {
      navigationBar.classList.remove('navigation-bar--design2')
    }
  }

  handleNavigationBarType = () => {
    const navigationBar = document.querySelector('.navigation-bar')

    if (window.matchMedia("(min-width: 768px)").matches) {
      navigationBar.classList.add('navigation-bar--desktop')
      navigationBar.classList.remove('navigation-bar--mobile')
    } else {
      navigationBar.classList.remove('navigation-bar--desktop')
      navigationBar.classList.add('navigation-bar--mobile')
    }
  } 

  handleNavigationBarMenuToggle = () => {
    const navigationBarMenuToggle = document.querySelector('.navigation-bar__menu-toggle')
    const navigationBarMenu = document.querySelector('.navigation-bar__menu')

    navigationBarMenuToggle.addEventListener('click', (event) => {
      event.currentTarget.classList.toggle('navigation-bar__menu-toggle--open')
      navigationBarMenu.classList.toggle('navigation-bar__menu--open')
    })
  }

  handleMobileLayout = () => {
    const navigationBar = document.querySelector('.navigation-bar')

    if ( navigationBar.classList.contains('navigation-bar--mobile') ) {
      this.removeBtnExpandMenuOnMobile()
      this.addBtnExpandMenuOnMobile()
      this.handleBtnExpandMenuOnMobile()
    } else {
      this.removeBtnExpandMenuOnMobile()
    }
  }

  handleBtnExpandMenuOnMobile = () => {
    const navigationBarMenu = document.querySelector('.navigation-bar__menu')
    const btnExpandMenu = navigationBarMenu.querySelectorAll('.btn--expand-menu')

    for ( var a = 0; a < btnExpandMenu.length; a++ ) {4
      btnExpandMenu[a].addEventListener('click', function() {
        this.parentNode.classList.toggle('menu-item-show-children')
      })
    }
  }

  addBtnExpandMenuOnMobile = () => {
    const navigationBarMenu = document.querySelector('.navigation-bar__menu')
    const menuWithSubMenu = navigationBarMenu.querySelectorAll('ul.menu > li.menu-item-has-children')

    let forEach = (array, callback, scope) => {
      for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]);
      }
    };

    forEach(menuWithSubMenu, (index, value) => {
      let btn = document.createElement("BUTTON")
      btn.innerHTML = '<span></span><span></span>';
      btn.classList.add('btn--expand-menu')

      value.appendChild(btn)
    });
  }

  removeBtnExpandMenuOnMobile = () => {
    const btnExpandMenu = document.querySelectorAll('.btn--expand-menu')

    for ( var a = 0; a < btnExpandMenu.length; a++ ) {
      btnExpandMenu[a].remove()
    }
  }
}