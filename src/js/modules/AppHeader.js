export default class NavigationBar {
  constructor(args) {
    this.init(args)
  }

  init = (args) => {
    if ( args.enabled ) {
      this.handleAppHeaderType()
    }
  }

  handleAppHeaderType = () => {
    let lastScrollTop = 0

    window.addEventListener("scroll", () => {
      const appHeader = document.querySelector(".app-header")
      let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0

      if (scrollTop > lastScrollTop && scrollTop > appHeader.offsetHeight){
      // if (scrollTop > lastScrollTop){
        appHeader.classList.add('app-header--slide-up')
      } else {
        if(scrollTop + window.innerHeight < document.body.scrollHeight) {
          appHeader.classList.remove('app-header--slide-up')
        }
      }

      lastScrollTop = scrollTop
    })
  }
}