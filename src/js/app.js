import './polyfill'
import AppHeader from './modules/AppHeader'
import NavigationBar from './modules/NavigationBar'
import '../sass/app.scss'

const InitNavigationBar = new NavigationBar()

const InitAppHeader = new AppHeader({
  enabled: true
})