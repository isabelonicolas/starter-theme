<?php get_header() ?>
	<div class="container">
		<div class="app-content">
			<h1>Error 404</h1>
			<p>It looks like nothing was found at this location.</p>
		</div>
	</div>
<?php get_footer() ?>