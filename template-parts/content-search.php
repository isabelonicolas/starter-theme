<article <?php post_class('post-entry') ?>>
	<header class="post-entry__header">
		<h2 class="post-entry__title"><?php the_title() ?></h2>
		<span class="post-entry__date">Posted on <?php the_date() ?></span>
	</header>	
	<main class="post-entry__main">
		<p class="post-entry__excerpt"><?php echo get_the_excerpt() ?></p>
		<span class="post-entry__readmore"><a href="<?php the_permalink() ?>">Continue reading →</a></span>
	</main>
</article>