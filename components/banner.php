<?php if ( class_exists('ACF') ) : ?>
  <?php
    $banner_content = '';
    $banner_background_image = ( get_field( 'banner_background_image' ) ? get_field( 'banner_background_image' ) : '' ); 
    
    if ( is_404() ) 
      $banner_content = '<h1>Page Not Found.</h1>';
    elseif ( is_search() )
      $banner_content = '<h1>Search Results for: ' . $_GET['s'] . '</h1>';
    elseif ( is_archive() )
      $banner_content = '<h1>' . get_the_archive_title() . '</h1>';
    elseif ( is_home() ) 
      $banner_content = '<h1>' . get_post_field( 'post_title', get_option( 'page_for_posts' ) ) . '</h1>';       
    else
      $banner_content = ( get_field( 'banner_content') ? get_field( 'banner_content' ) : '<h1>' . get_the_title() . '</h1>' ); 
  ?>
  <div class="banner">
    <?php if ( $banner_background_image ) : ?>
      <div class="banner__background-image" style="background-image: url(<?php echo $banner_background_image; ?>)"></div>
    <?php endif; ?>
    <div class="banner__background-overlay" data-opacity></div>
    <div class="banner__background-content">
      <div class="container">
        <?php echo $banner_content; ?>
      </div>
    </div>
  </div>
<?php else: ?>
  <p><em>Banner Component needs <strong>Advanced Custom Field Pro</strong> as a dependency plugin</em>. Please install the required plugin.</p>
<?php endif; ?>