/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/public";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./polyfill */ \"./src/js/polyfill.js\");\n/* harmony import */ var _polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_polyfill__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modules_AppHeader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/AppHeader */ \"./src/js/modules/AppHeader.js\");\n/* harmony import */ var _modules_NavigationBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/NavigationBar */ \"./src/js/modules/NavigationBar.js\");\n/* harmony import */ var _sass_app_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sass/app.scss */ \"./src/sass/app.scss\");\n/* harmony import */ var _sass_app_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sass_app_scss__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nvar InitNavigationBar = new _modules_NavigationBar__WEBPACK_IMPORTED_MODULE_2__[\"default\"]();\nvar InitAppHeader = new _modules_AppHeader__WEBPACK_IMPORTED_MODULE_1__[\"default\"]({\n  enabled: true\n});\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/js/modules/AppHeader.js":
/*!*************************************!*\
  !*** ./src/js/modules/AppHeader.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return NavigationBar; });\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar NavigationBar = function NavigationBar(_args) {\n  var _this = this;\n\n  _classCallCheck(this, NavigationBar);\n\n  this.init = function (args) {\n    if (args.enabled) {\n      _this.handleAppHeaderType();\n    }\n  };\n\n  this.handleAppHeaderType = function () {\n    var lastScrollTop = 0;\n    window.addEventListener(\"scroll\", function () {\n      var appHeader = document.querySelector(\".app-header\");\n      var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;\n\n      if (scrollTop > lastScrollTop && scrollTop > appHeader.offsetHeight) {\n        // if (scrollTop > lastScrollTop){\n        appHeader.classList.add('app-header--slide-up');\n      } else {\n        if (scrollTop + window.innerHeight < document.body.scrollHeight) {\n          appHeader.classList.remove('app-header--slide-up');\n        }\n      }\n\n      lastScrollTop = scrollTop;\n    });\n  };\n\n  this.init(_args);\n};\n\n\n\n//# sourceURL=webpack:///./src/js/modules/AppHeader.js?");

/***/ }),

/***/ "./src/js/modules/NavigationBar.js":
/*!*****************************************!*\
  !*** ./src/js/modules/NavigationBar.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return NavigationBar; });\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar NavigationBar = function NavigationBar(args) {\n  var _this = this;\n\n  _classCallCheck(this, NavigationBar);\n\n  this.init = function (args) {\n    _this.windowEvents(); // console.log(args)\n\n  };\n\n  this.windowEvents = function () {\n    window.addEventListener(\"load\", _this.handleAppHeaderType);\n    window.addEventListener(\"scroll\", _this.handleAppHeaderType);\n    window.addEventListener(\"load\", _this.handleNavigationBarType);\n    window.addEventListener(\"resize\", _this.handleNavigationBarType);\n    window.addEventListener(\"load\", _this.handleNavigationBarDesign);\n    window.addEventListener(\"scroll\", _this.handleNavigationBarDesign);\n    window.addEventListener('load', _this.handleNavigationBarMenuToggle);\n    window.addEventListener('load', _this.handleMobileLayout);\n    window.addEventListener('resize', _this.handleMobileLayout);\n  };\n\n  this.handleAppHeaderType = function () {};\n\n  this.handleNavigationBarDesign = function () {\n    var appHeader = document.querySelector(\".app-header\"),\n        navigationBar = document.querySelector(\".navigation-bar\");\n    var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,\n        height = appHeader.offsetHeight;\n\n    if (scrollTop > height) {\n      navigationBar.classList.add('navigation-bar--design2');\n    } else {\n      navigationBar.classList.remove('navigation-bar--design2');\n    }\n  };\n\n  this.handleNavigationBarType = function () {\n    var navigationBar = document.querySelector('.navigation-bar');\n\n    if (window.matchMedia(\"(min-width: 768px)\").matches) {\n      navigationBar.classList.add('navigation-bar--desktop');\n      navigationBar.classList.remove('navigation-bar--mobile');\n    } else {\n      navigationBar.classList.remove('navigation-bar--desktop');\n      navigationBar.classList.add('navigation-bar--mobile');\n    }\n  };\n\n  this.handleNavigationBarMenuToggle = function () {\n    var navigationBarMenuToggle = document.querySelector('.navigation-bar__menu-toggle');\n    var navigationBarMenu = document.querySelector('.navigation-bar__menu');\n    navigationBarMenuToggle.addEventListener('click', function (event) {\n      event.currentTarget.classList.toggle('navigation-bar__menu-toggle--open');\n      navigationBarMenu.classList.toggle('navigation-bar__menu--open');\n    });\n  };\n\n  this.handleMobileLayout = function () {\n    var navigationBar = document.querySelector('.navigation-bar');\n\n    if (navigationBar.classList.contains('navigation-bar--mobile')) {\n      _this.removeBtnExpandMenuOnMobile();\n\n      _this.addBtnExpandMenuOnMobile();\n\n      _this.handleBtnExpandMenuOnMobile();\n    } else {\n      _this.removeBtnExpandMenuOnMobile();\n    }\n  };\n\n  this.handleBtnExpandMenuOnMobile = function () {\n    var navigationBarMenu = document.querySelector('.navigation-bar__menu');\n    var btnExpandMenu = navigationBarMenu.querySelectorAll('.btn--expand-menu');\n\n    for (var a = 0; a < btnExpandMenu.length; a++) {\n      4;\n      btnExpandMenu[a].addEventListener('click', function () {\n        this.parentNode.classList.toggle('menu-item-show-children');\n      });\n    }\n  };\n\n  this.addBtnExpandMenuOnMobile = function () {\n    var navigationBarMenu = document.querySelector('.navigation-bar__menu');\n    var menuWithSubMenu = navigationBarMenu.querySelectorAll('ul.menu > li.menu-item-has-children');\n\n    var forEach = function forEach(array, callback, scope) {\n      for (var i = 0; i < array.length; i++) {\n        callback.call(scope, i, array[i]);\n      }\n    };\n\n    forEach(menuWithSubMenu, function (index, value) {\n      var btn = document.createElement(\"BUTTON\");\n      btn.innerHTML = '<span></span><span></span>';\n      btn.classList.add('btn--expand-menu');\n      value.appendChild(btn);\n    });\n  };\n\n  this.removeBtnExpandMenuOnMobile = function () {\n    var btnExpandMenu = document.querySelectorAll('.btn--expand-menu');\n\n    for (var a = 0; a < btnExpandMenu.length; a++) {\n      btnExpandMenu[a].remove();\n    }\n  };\n\n  this.init(args);\n};\n\n\n\n//# sourceURL=webpack:///./src/js/modules/NavigationBar.js?");

/***/ }),

/***/ "./src/js/polyfill.js":
/*!****************************!*\
  !*** ./src/js/polyfill.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * Polyfill for .remove function\n */\nElement.prototype.remove = function () {\n  this.parentElement.removeChild(this);\n};\n\nNodeList.prototype.remove = HTMLCollection.prototype.remove = function () {\n  for (var i = this.length - 1; i >= 0; i--) {\n    if (this[i] && this[i].parentElement) {\n      this[i].parentElement.removeChild(this[i]);\n    }\n  }\n};\n/**\n * Polyfill for .foreach function\n */\n// if (window.NodeList && !NodeList.prototype.forEach) {\n//   NodeList.prototype.forEach = function (callback, thisArg) {\n//       thisArg = thisArg || window;\n//       for (var i = 0; i < this.length; i++) {\n//           callback.call(thisArg, this[i], i, this);\n//       }\n//   };\n// }\n\n//# sourceURL=webpack:///./src/js/polyfill.js?");

/***/ }),

/***/ "./src/sass/app.scss":
/*!***************************!*\
  !*** ./src/sass/app.scss ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/sass/app.scss?");

/***/ })

/******/ });